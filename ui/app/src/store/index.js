import Vue from 'vue';
import Vuex from 'vuex'
import axios from 'axios';
import env from '../../env'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        info: null,
        error: false,
    },
    getters: {
        INFO: state => state.info,
        ERROR: state => state.error,
    },
    actions: {
        getData({commit}, params) {
            axios
                .post(`${env.API_URL}/get-weather/`, {
                    address: params.address,
                    formatted_address: params.formatted_address
                })
                .then(response => {
                    commit('SET_INFO', response.data)
                }).catch((error) => {

                commit('SET_ERROR', true)


            });
        }
    },
    mutations: {
        SET_INFO(state, data) {
            state.info = data
        },
        SET_ERROR(state, data) {
            state.error = data
        }
    },
})