<?php

namespace App\Mosdels\API;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Weather
 * @property string $location
 * @property string $temp
 * @property string $name
 * @package App\Mosdels\API
 */
class Weather extends Model
{
    /**
     * @var string
     */
    protected $table = 'weather';

    /**
     * @var string[]
     */
    protected $fillable = ['location','temp'];

}
