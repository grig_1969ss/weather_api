<?php

namespace App\Repositories;

use App\Mosdels\API\Weather;
use App\Repositories\Interfaces\WeatherInterface;
use GuzzleHttp\Client;

class WeatherRepository implements WeatherInterface
{
    /**
     *
     */
    const API_URL = "https://openweathermap.org/data/2.5/weather";

    public function getWeather($address)
    {
        $client = new Client();
        $response = $client->request(
            'GET',
            static::API_URL . "?q={$address}&units=metric&appid=". config('app.open_weather')
        );

        return json_decode($response->getBody());
    }

    /**
     * @param Weather $weather
     * @return Weather
     */
    public function storeWeather($weather): Weather
    {
        return Weather::create([
            'location' => $weather->name,
            'temp' => $weather->main->temp
        ]);
    }

}
