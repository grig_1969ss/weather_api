<?php
namespace App\Repositories\Interfaces;

interface WeatherInterface
{
    public function getWeather($address);

    public function storeWeather($weather);
}
