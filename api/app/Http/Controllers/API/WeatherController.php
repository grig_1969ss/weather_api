<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Repositories\WeatherRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class WeatherController extends Controller
{
    /*
     *
     */
    public $weatherRepository;

    /**
     * WeatherController constructor.
     * @param WeatherRepository $weatherRepository
     */
    public function __construct(WeatherRepository $weatherRepository)
    {
        $this->weatherRepository = $weatherRepository;
    }

    /**
     * @return JsonResponse
     */
    public function getWeather(): JsonResponse
    {
        $weatherResponse = $this->weatherRepository->getWeather((string)request()->address);

        $info = $this->weatherRepository->storeWeather($weatherResponse);

        return response()->json([
            'location' => $info->location,
            'temperature' => $info->temp,
            'type' => '°C'
        ], Response::HTTP_OK);

    }

}
